# codingchallenge-frontend

## Description
Simple Reactjs app with sign-up and login functionality.

## Stack
* Reactjs
* Redux
* Axios
* notistack snackbars

## Code Setup

* `yarn install`
* `yarn start`