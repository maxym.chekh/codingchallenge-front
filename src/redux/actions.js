import {
  CURRENT_USER_LOAD_SUCCESS,
  SIGN_OUT,
} from "./actionTypes"

export const currentUserLoadSuccess = currentUser => ({
  type: CURRENT_USER_LOAD_SUCCESS,
  currentUser
})

export const signOut = () => ({
  type: SIGN_OUT
})