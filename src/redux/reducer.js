import { combineReducers } from "redux"

import {
  CURRENT_USER_LOAD_SUCCESS,
  SIGN_OUT,
  SET_ERROR_MESSAGE,
  REMOVE_ERROR_MESSAGE,
  SET_INFO_MESSAGE,
  REMOVE_INFO_MESSAGE,
} from "./actionTypes"

const initialState = {
  currentUser: null,
  errorMessage: null,
  infoMessage: null
}

const currentUser = (state = initialState.currentUser, action) => {
  switch (action.type) {
    case CURRENT_USER_LOAD_SUCCESS:
      return action.currentUser
    case SIGN_OUT:
      state = null
      return state
    default:
      return state
  }
}

const errorMessage = (state = initialState.errorMessage, action) => {
  switch (action.type) {
    case SET_ERROR_MESSAGE:
      return action.errorMessage
    case REMOVE_ERROR_MESSAGE:
      return null
    default:
      return state
  }
}

const infoMessage = (state = initialState.infoMessage, action) => {
  switch (action.type) {
    case SET_INFO_MESSAGE:
      return action.infoMessage
    case REMOVE_INFO_MESSAGE:
      return null
    default:
      return state
  }
}

const reducer = combineReducers({
  currentUser,
  errorMessage,
  infoMessage
})

export default reducer
