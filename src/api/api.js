import axios from "axios";
import store from "./../store";
import { REMOVE_ERROR_MESSAGE, SET_ERROR_MESSAGE } from "../redux/actionTypes";

const JWT_TOKEN_KEY = "jwtToken";
const hostname = window && window.location && window.location.hostname;

function isLaunchedLocally(hostname) {
  return hostname === "localhost" || hostname === "127.0.0.1";
}

const apiRoot = isLaunchedLocally(hostname)
  ? "http://localhost:3000"
  : hostname.includes("api-route") || hostname.includes("api-prod-root")
    ? "https://" + hostname
    : process.env.REACT_APP_API_ROOT || "http://localhost:3000";

const authRoot = isLaunchedLocally(hostname)
  ? "http://localhost:4000"
  : hostname.includes("auth-route") || hostname.includes("auth-prod-root")
    ? "https://" + hostname
    : process.env.REACT_APP_AUTH_ROOT || "http://localhost:4000";

class Api {
  constructor(serviceRoot) {
    this.serviceRoot = serviceRoot;
  }

  static get jwtHeader() {
    const jwt = JSON.parse(localStorage.getItem(JWT_TOKEN_KEY));
    return jwt && jwt.accessToken
      ? { Authorization: `Bearer ${jwt.accessToken}` }
      : {};
  }

  static set jwt(jwtToken) {
    if (jwtToken) {
      localStorage.setItem(JWT_TOKEN_KEY, JSON.stringify(jwtToken));
    } else {
      localStorage.removeItem(JWT_TOKEN_KEY);
    }
  }

  async getPage(url, page, pageSize, params) {
    return await this.get(url, { page, pageSize, ...params }, { headers: Api.jwtHeader });
  }

  async handleError(actionF) {
    try {
      return (await actionF()).data;
    } catch (err) {
      const resp = err.response || {}
      const statusCode = resp.status
      const data = resp.data || {}
      const originalRequest = err.config
      const jwt = JSON.parse(localStorage.getItem(JWT_TOKEN_KEY))
      if (
        err.response.status === 401 &&
        jwt && jwt.refreshToken &&
        !originalRequest.__isRetryRequest
      ) {
        return await this.refreshTokenAndRetry(originalRequest)
      } else {
        let message = data.message || err.message
        if (statusCode >= 400 && statusCode < 500 && data.errors) {
          message = data.errors.map((e) => e.msg).join("\n")
        }
        store.dispatch({ type: REMOVE_ERROR_MESSAGE })
        const dontShow = statusCode === 401
        if (!dontShow) {
          store.dispatch({ type: SET_ERROR_MESSAGE, errorMessage: message })
        }
        throw err;
      }
    }
  }

  async refreshTokenAndRetry(originalRequest) {
    try {
      originalRequest.__isRetryRequest = true
      const jwt = JSON.parse(localStorage.getItem(JWT_TOKEN_KEY))
      const tokenResponse = await axios.post(`${authRoot}/auth/refreshToken`, {
        refreshToken: jwt.refreshToken 
      })
      if (tokenResponse.status === 200) {
        setJwtToken(tokenResponse.data)
        originalRequest.headers['Authorization'] = `Bearer ${tokenResponse.data.accessToken}`
        return await this.handleError(
          () => axios(originalRequest)
        );
      }
    } catch (tokenErr) {
      store.dispatch({ type: SET_ERROR_MESSAGE, errorMessage: "Session expired or invalid" })
      setJwtToken(null);
      throw tokenErr
    }
  }

  async get(url, params) {
    return await this.handleError(() =>
      axios.get(`${this.serviceRoot}/${url}`, {
        params,
        headers: Api.jwtHeader,
      })
    );
  }

  async post(url, payload, contentType) {
    return await this.handleError(() =>
      axios.post(`${this.serviceRoot}/${url}`, payload, {
        withCredentials: true,
        headers: {
          "Content-Type": contentType || "application/json",
          ...Api.jwtHeader,
        },
      })
    );
  }

  async put(url, payload) {
    return await this.handleError(() =>
      axios.put(`${this.serviceRoot}/${url}`, payload, {
        withCredentials: true,
        headers: Api.jwtHeader,
      })
    );
  }

  async delete(url) {
    return await this.handleError(() =>
      axios.delete(`${this.serviceRoot}/${url}`, {
        withCredentials: true,
        headers: Api.jwtHeader,
      })
    );
  }
}

export const setJwtToken = (jwtData) => (Api.jwt = jwtData);

export const authApi = new Api(`${authRoot}/auth`);
export const mainApi = new Api(apiRoot);
export const API_URL = apiRoot;
export const AUTH_URL = authRoot;
