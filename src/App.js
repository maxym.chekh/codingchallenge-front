import React, { useEffect } from "react"
import { Redirect, Route, Switch, withRouter } from "react-router-dom"
import Login from "./components/auth/Login"
import SignUpContainer from "./components/auth/SignUpContainer"
import { connect } from "react-redux"
import { mainApi } from "./api/api"
import { currentUserLoadSuccess } from "./redux/actions"
import Home from "./components/Home"
import { withSnackbar } from "notistack"
import { useSnackbar } from "notistack"

const mapStateToProps = state => ({
  currentUser: state.currentUser,
  errorMessage: state.errorMessage,
  infoMessage: state.infoMessage
})

const mapDispatchToProps = dispatch => ({
  getCurrentUser: async history => {
    try {
      const currentUser = await mainApi.get("profile")
      dispatch(currentUserLoadSuccess(currentUser))
    } catch (e) {
      //dispatch(currentUserIsEmpty())
      history.push("/login")
    }
  }
})


const App = (props) => {
  const { getCurrentUser, currentUser, errorMessage, infoMessage } = props

  useEffect(() => {
    getCurrentUser(props.history)
  }, [])

  const { enqueueSnackbar } = useSnackbar()
  useEffect(() => {
    if (errorMessage) enqueueSnackbar(errorMessage, { 
      variant: "error",
      style: { whiteSpace: 'pre' }
    })
  }, [errorMessage])

  useEffect(() => {
    if (infoMessage) enqueueSnackbar(infoMessage, { 
      style: { whiteSpace: 'pre' }
    })
  }, [infoMessage])

  return (
    <main>
      <Switch>
        <Route
          exact
          path="/"
          render={() => {
            if (currentUser) return <Redirect to="/home" />
          }}
        />
        <Route exact path="/home" component={Home} />
        <Route
          exact
          path="/login"
          render={(routeProps) => currentUser ? <Redirect to="/home" /> : <Login {...routeProps}/>}
        />
        <Route exact path="/sign-up" component={SignUpContainer} />
      </Switch>
    </main>
  )
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(withSnackbar(App)))