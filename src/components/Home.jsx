import React from "react"
import { connect } from "react-redux"
import { makeStyles } from "@material-ui/core"
import { withRouter, Link as RouterLink } from "react-router-dom"
import { signOut } from "../redux/actions"
import Link from "@material-ui/core/Link"
import { setJwtToken } from "../api/api"

const mapStateToProps = state => ({
  currentUser: state.currentUser
})

const mapDispatchToProps = dispatch => ({
  logout: async () => {
    setJwtToken(null)
    dispatch(signOut())
  }
})

const useStyles = makeStyles(theme => ({
  welcomePage: {
    textAlign: 'center',
    fontSize: '28px',
    marginTop: '50px'   
  }
}))

const Home = (props) => {
  const { currentUser, logout } = props
  const classes = useStyles()
  const handleNameClick = async () => {
    await logout()

    props.history.push("/login")
  }

  return (
    <div className={classes.welcomePage}>
      Welcome, <Link to="/" onClick={async () => await handleNameClick()} component={RouterLink}>
        {currentUser && currentUser.fullName}
      </Link> 
    </div>
  )
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Home))
