import React from "react"
import SignUp from "./SignUp"
import { authApi } from "../../api/api"
import { connect } from "react-redux"
import { SET_INFO_MESSAGE } from "../../redux/actionTypes"

const mapDispatchToProps = dispatch => ({
  signUp: async signUpData => {
    try {
      await authApi.post("registration", signUpData)
      dispatch({ type: SET_INFO_MESSAGE, infoMessage: "Succesfully signed up" })
    } catch (e) {
      throw e
    }
  }
})

const SignUpContainer = ({ history, signUp }) => {
  const handleSignUp = async signUpData => {
    await signUp(signUpData)
    history.push("/login")
  }

  return (
    <div>
      <SignUp onSignUp={handleSignUp} />
    </div>
  )
}

export default connect(
  null,
  mapDispatchToProps
)(SignUpContainer)
