import React, { useState } from "react"
import { connect } from "react-redux"
import { authApi, mainApi, setJwtToken } from "../../api/api"
import Button from "@material-ui/core/Button"
import FilledInput from "@material-ui/core/FilledInput"
import { Link as RouterLink } from "react-router-dom"
import Link from "@material-ui/core/Link"
import { makeStyles } from "@material-ui/core/styles"
import { currentUserLoadSuccess } from "../../redux/actions"

const mapDispatchToProps = dispatch => ({
  login: async loginData => {
    try {
      const jwtData = await authApi.post("login", loginData)
      setJwtToken(jwtData)
      const currentUser = await mainApi.get("profile")
      dispatch(currentUserLoadSuccess(currentUser))
      return currentUser
    } catch (e) {
      throw e
      //console.error("Error: ", e)
    }
  }
})

const useStyles = makeStyles(theme => ({
  container: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    height: "100vh"
  },
  textField: {
    marginTop: theme.spacing(2),
    width: "24vmax",
    minWidth: "250px"
  },
  button: {
    marginTop: theme.spacing(2),
    width: "24vmax",
    minWidth: "250px"
  },
  logoImage: {
    width: "24vmax",
    marginBottom: "3rem"
  },
  link: {
    marginTop: theme.spacing(1)
  }
}))

const Login = ({ login, history }) => {
  const classes = useStyles()
  const [email, setEmail] = useState(null)
  const [password, setPassword] = useState(null)

  const handleLoginClick = async ({ email, password }) => {
    try {
      const currentUser = await login({ email, password })
      history.push("/")
    } catch (e) {
      console.error(e)
    }
  }

  return (
    <form
      className={classes.container}
      noValidate
      onSubmit={async e => {
        e.preventDefault()
        await handleLoginClick({ email, password })
      }}
    >

      <FilledInput
        id="sign-in-email"
        label="Email"
        className={classes.textField}
        type="text"
        name="login"
        placeholder="email"
        variant="outlined"
        value={email || ""}
        onChange={event => setEmail(event.target.value)}
      />
      <FilledInput
        id="sign-in-password"
        label="Password"
        className={classes.textField}
        type="password"
        name="password"
        placeholder="password"
        variant="outlined"
        value={password || ""}
        onChange={event => setPassword(event.target.value)}
      />
      <Button
        type="submit"
        variant="contained"
        color="primary"
        className={classes.button}
        size="large"
      >
        Log In
      </Button>
      <Link className={classes.link} component={RouterLink} to="/sign-up">
        Sign Up
      </Link>
    </form>
  )
}

export default connect(
  null,
  mapDispatchToProps
)(Login)
