const strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{10,})", "g")
const mediumRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})")
// minimum 8 characters with at least one number and one character
const enoughRegex = new RegExp("^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}")
const emailRegex = new RegExp(
  "^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$"
)
const nameRegex = /^[a-zA-Z]{5,}/

export const validatePassword = password => {
  let result
  if (password.length === 0) {
    result = 0
  } else if (strongRegex.test(password)) {
    result = 3
  } else if (mediumRegex.test(password)) {
    result = 2
  } else if (enoughRegex.test(password)) {
    result = 1
  } else if (!enoughRegex.test(password)) {
    result = -1
  }
  return result
}

export const validatePasswordText = password => {
  switch (validatePassword(password)) {
    case 0:
      return ""
    case 1:
      return "Weak!"
    case 2:
      return "Medium!"
    case 3:
      return "Strong!"
    default:
      return "Must be 8 or more characters, inlude at least 1 number and 1 letter"
  }
}

export const validateEmail = email => {
  return emailRegex.test(email)
}

export const validateName = name => {
  return nameRegex.test(name.replace(/\s/g,''));
}