import React, { useState } from "react"
import TextField from "@material-ui/core/TextField"
import Button from "@material-ui/core/Button"
import { Link as RouterLink } from "react-router-dom"
import Link from "@material-ui/core/Link"
import { makeStyles } from "@material-ui/core"
import { validateEmail, validatePassword, validatePasswordText, validateName } from "./auth-validation"

const useStyles = makeStyles(theme => ({
  container: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    height: "100vh"
  },
  textField: {
    marginTop: theme.spacing(2),
    width: "24vmax",
    minWidth: "270px"
  },
  button: {
    marginBottom: theme.spacing(1),
    marginTop: theme.spacing(2),
    width: "24vmax",
    minWidth: "270px"
  },
  logoImage: {
    width: "12vmax",
    marginBottom: "2rem"
  }
}))


const SignUp = ({ onSignUp }) => {
  const classes = useStyles()
  const [fullName, setFullName] = useState("")
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")
  const [confirmPassword, setConfirmPassword] = useState("")
  const [validateError, setValidateError] = useState({})

  const validateAndSubmitForm = async () => {
    const isNameValid = validateName(fullName);
    const isEmailValid =  validateEmail(email);
    const isPasswordValid = validatePassword(password) > 0;
    const passwordsMatch = password === confirmPassword;
    
    if (isNameValid && isEmailValid && isPasswordValid && passwordsMatch) {   
      try {
        await onSignUp({
          fullName,
          email,
          password
        })
      } catch (e) {
        console.error("Error: ", e)
        //throw e
      }
    } else {
      setValidateError({
        fullName: !isNameValid,
        email: !isEmailValid,
        password: !isPasswordValid,
      })
    }
  }
  const confirmPasswordError = password.length > 0 && password !== confirmPassword

  return (
    <form
      className={classes.container}
      noValidate
      onSubmit={async e => {
        e.preventDefault()
        await validateAndSubmitForm()
      }}
    >

      <TextField
        error={validateError.email}
        helperText={validateError.email && "Email is not valid"}
        id="sign-up-email"
        label="Email"
        className={classes.textField}
        type="text"
        name="login"
        placeholder="Email"
        autoComplete="login"
        variant="outlined"
        value={email}
        onChange={event => {
          validateError.email = null
          setEmail(event.target.value)
        }}
      />
      <TextField
        error={validateError.fullName}
        helperText={validateError.fullName && "Name must be at least 5 characters long"}
        id="sign-up-full-name"
        label="Full name"
        className={classes.textField}
        type="ignore"
        name="full-name"
        placeholder="Full name"
        variant="outlined"
        value={fullName}
        onChange={event => {
          validateError.fullName = null
          setFullName(event.target.value)
        }}
      />
      <TextField
        error={validateError.password}
        id="sign-up-password"
        label="Password"
        className={classes.textField}
        type="password"
        name="password"
        placeholder="Password"
        autoComplete="new-password"
        variant="outlined"
        value={password}
        helperText={validatePasswordText(password)}
        onChange={event => {
          validateError.password = null
          setPassword(event.target.value)
        }}
      />
      <TextField
        id="sign-up-confirm-password"
        error={confirmPasswordError}
        label="Confirm password"
        className={classes.textField}
        autoComplete="new-password"
        type="password"
        name="password"
        placeholder="Confirm password"
        variant="outlined"
        value={confirmPassword}
        helperText={confirmPasswordError && "Passwords do not match"}
        onChange={event => setConfirmPassword(event.target.value)}
      />
      <Button
        type="submit"
        variant="contained"
        color="primary"
        className={classes.button}
        size="large"
      >
        Sign up
      </Button>
      <Link component={RouterLink} to="/login">
        Log In
      </Link>
    </form>
  )
}

export default SignUp
